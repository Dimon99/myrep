package Xc;

import java.io.*;
import java.util.Scanner;

public class WriteToBase {
  public static void writeBase(File logs,File base) throws IOException{
	  FileWriter fw=new FileWriter(base, true);
	  Scanner sc=new Scanner(logs);
	  String line=sc.nextLine();
	  
	  fw.write(line+"\n");
	  fw.flush();
	  fw.close();
	  }
}
